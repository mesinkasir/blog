---
layout: post
title:  "Welcome to Mesin kasir Jekyll present by gitlab"
date:   2020-07-10 15:32:14 -0300
categories: mesin kasir jekyll blog
---

![Image of Yaktocat](https://assets.gitlab-static.net/assets/logo-d36b5212042cebc89b96df4bf6ac24e43db316143e89926c0db839ff694d2de4.svg)

### Selamat datang di mesin kasir blogs

Persembahan dari jekyll dengan gitlab untuk mendevelope site ini.
Alasan menggunakan jekyll karena kemudahan nya untuk memberikan informasi mengenai blog update informasi technology aplikasi dan software hingga mesin kasir system plus pembuatan apk website untuk kebutuhan usaha.

Nyaman menggunakan [jekyll](https://jekyllrb.com) untuk media bloging platform maupun untuk build dan develope modern website , jekyll salah satu leader populer generasi terbaru static site generator, kecepatan akses dan load yang luar biasa menjadikan nya prioritas utama bagi para blogger. selain itu dengan liquid dan menggunakan ruby on rails menyempurnakan website dari berbagai faktor mulai dari kecepatan, struktur , hingga kebutuhan untuk develope themes sesuai kebutuhan.

Dan web blog informasi mengenai mesin kasir ini tentu saja dipersembahkan oleh [gitlab](https://gitlab.com), sama hal nya seperti github gitlab memberikan kemudahan dalam deploy website blog statis untuk kebutuhan host, mudah dalam mendeploy secara cepat dengan gitlab pages.

Maksud dan tujuan dalam pembuatan website blog ini agar kami dapat memberikan informasi update terbaru mengenai layanan dan produk kami terkait dengan aplikasi kasir, aplikasi toko, aplikasi restoran, software program, mesin kasir dan jasa pembuatan website modern yang berguna untuk kebutuhan usaha dan bisnis.

Karena dipersembahkan oleh gitlab dan jekyll maka kali ini kita akan menjelaskan apa itu gitlab dan jekyll.

### GITLAB

Gitlab adalah wadah bagi programer dan developer untuk kebutuhan berbagi akan project mereka , mulai dari aplikasi hingga web framework smeua developer dunia berkumpul disini untuk berkontribusi dalam dunia technology. selain itu gitlab memberikan kebutuhan terbaik dalam hosting untuk deploy static site agar dapat diakses secara penuh oleh user. dan tentu saja kamu dapat menggunakan gitlab premium untuk kebutuhan develope project besar kamu bersama team member dalam kebutuhan project. gitlab memudahkan untuk melakukan remote via git dan berbagai aktivitas lain nya memungkinkan secara cepat bekerja dengan gitbash maupun yarn dan npm.

### JEKYLL 

Kamu blogger pasti tidak akan asing dengan jekyll, yap.. generasi bloging platform terbaru ini menjelma menjadi populer bersama hugo dan hexo, plus pasukan javascript seperti gatsby dan angular.dan tidak dipungkiri bahwa hadirnya jekyll mampu membawa static site menjadi pilihan terbaik developer, dan ini lah era kebangkitan static site setelah sekian lama tertidur oleh perkasa nya dinamis site yang dikuasai oleh wordpress, drupal maupun joomla.
Tidak ada salah nya untuk belajar membuat blog dan website dari jekyll ini, selain kamu mendapat ilmu pengetahuan yang baru tentu saja skill pemorgraman kamu akan bertambah, stop dan jangan berhenti pada zona nyaman kamu dengan old school blog mu, lakukan loncatan dengan mencoba mendevelope web atau blog dengan platform terbaru khusus nya jekyll ini yang relatif lebih mudah dipahami .

ketika kamu ngebolg dengan blogspot maupun wordpress atau blog platform lain nya, maka kini saat nya deh coba upgrade ke blog modern ini, apa saja sih kelebihan nya ?? tentu saja ringan dan sangat cepat khusus nya pada load website, dan ada lagi yang lebih menarik, dimana kamu dapat update artikel dengan menggunakan markdown technology terbaru yang simple dan mudah untuk kamu pelajari dalam kebutuhan update artikel berita maupun post pada website blog mu, markdown semakin memungkinkan kecepatan dalam hal penulisan hingga insert media gambar, dengan markdown semua terhandle dengan mudah nya.

Kini saat nya uprage dan update website blog mu dengan modern technology menggunakan jekyll dengan host gitlab, dan jika kamu membutuhkan jasa pembuatan website modern maka kamu dapat hubungi team kami.

Seklias dan coretan selamat datang via blog mesin kasir persembahan dari jekyll feat gitlab